

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Game extends JPanel implements KeyListener,ActionListener{


    private int[] rabbitxLength=new int[750];
    private int[] rabbityLength= new int[750];
    private int[] heartxPos={30,60,90,120,150,180,210,240,270,300,330,360,390,420,450,480,510,540,570,600,630,660,690,720,750,780,810};
    private int[] heartyPos={70,100,130,160,190,220,250,280,310,340,370,400,430,460,490,520,550,580,600};
    private ImageIcon heartIcon;

    Random random=new Random();
    private int xpos=random.nextInt(27);
    private int ypos=random.nextInt(19);

    private int lengthOfFamily = 3;
    private boolean left=false;
    private boolean right=false;
    private boolean top=false;
    private boolean bottom=false;
    private ImageIcon topIcon;

    private ImageIcon topRabbit;
    private ImageIcon bottomRabbit;
    private ImageIcon
            rightRabbit;
    private ImageIcon leftRabbit;

    private Timer timer;
    private int delay=100;

    private int moves=0;
    private long start=0;

    private ImageIcon rabbitFamily;


    public Game() {
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer=new Timer(delay,this);
        timer.start();

    }

    public void paint(Graphics gr){


        if(moves==0){
            rabbitxLength[2]=30;
            rabbitxLength[1]=60;
            rabbitxLength[0]=90;

               rabbityLength[2]=100;
               rabbityLength[1]=100;
               rabbityLength[0]=100;

        }


        gr.setColor(Color.pink);
        gr.drawRect(20,10,850,55);

        topIcon= new ImageIcon("top.png");
        topIcon.paintIcon(this,gr,20,11);



        gr.setColor(Color.white);
        gr.drawRect(20,70,850,570);

        gr.setColor(Color.BLACK);
        gr.fillRect(21,71,849,568);

        gr.setColor(Color.WHITE);
        gr.setFont(new Font("arial",Font.PLAIN,14));
        if(moves!=0){
            long elapsedTime = System.currentTimeMillis()-start;
            gr.drawString("Timer: "+elapsedTime/1000,780,30);
        }

        rightRabbit=new ImageIcon("rabbit4.png");
        rightRabbit.paintIcon(this,gr,rabbitxLength[0],rabbityLength[0]);

         for(int i=0;i<lengthOfFamily;i++) {
             if (i == 0 && right) {
                 rightRabbit = new ImageIcon("rabbit4.png");
                 rightRabbit.paintIcon(this, gr, rabbitxLength[i], rabbityLength[i]);
             }

             if (i == 0 && left) {
                 leftRabbit = new ImageIcon("rabbit1.png");
                 leftRabbit.paintIcon(this, gr, rabbitxLength[i], rabbityLength[i]);
             }

             if (i == 0 && top) {
                 topRabbit = new ImageIcon("rabbit3.png");
                 topRabbit.paintIcon(this, gr, rabbitxLength[i], rabbityLength[i]);
             }

           if (i==0 && bottom) {
               bottomRabbit = new ImageIcon("rabbit2.png");
               bottomRabbit.paintIcon(this, gr, rabbitxLength[i], rabbityLength[i]);
           }

             if(i!=0){
                 rabbitFamily=new ImageIcon("rabbit.png");
                 rabbitFamily.paintIcon(this, gr, rabbitxLength[i], rabbityLength[i]);
             }
         }
             heartIcon=new ImageIcon("love.png");

         if(heartxPos[xpos]==rabbitxLength[0] && heartyPos[ypos]==rabbityLength[0]){
             lengthOfFamily++;
             xpos=random.nextInt(27);
             ypos=random.nextInt(19);
         }
         heartIcon.paintIcon(this,gr,heartxPos[xpos],heartyPos[ypos]);
         gr.dispose();
    }

    public void actionPerformed(ActionEvent e) {

        timer.start();
        if(right){

            for (int i=lengthOfFamily-1;i>=0;i--){
               rabbityLength[i+1]=rabbityLength[i];
            }
                for(int i=lengthOfFamily;i>=0;i--){
                if (i==0){
                         rabbitxLength[i]=rabbitxLength[i]+30;
                }  else{
                      rabbitxLength[i]=rabbitxLength[i-1];
                }
                  if(rabbitxLength[i]>810){
                    rabbitxLength[i]=30;
                  }
                }

            repaint();
        }else if(left){
                 for (int i=lengthOfFamily-1;i>=0;i--){
                    rabbityLength[i+1]=rabbityLength[i];
                 }
                     for(int i=lengthOfFamily;i>=0;i--){
                     if (i==0){
                              rabbitxLength[i]=rabbitxLength[i]-30;
                     }  else{
                           rabbitxLength[i]=rabbitxLength[i-1];
                     }
                       if(rabbitxLength[i]<30){
                         rabbitxLength[i]=810;
                       }
                     }

                 repaint();                                             
        }else if(top){
              for (int i=lengthOfFamily-1;i>=0;i--){
                 rabbitxLength[i+1]=rabbitxLength[i];
              }
                  for(int i=lengthOfFamily;i>=0;i--){
                  if (i==0){
                           rabbityLength[i]=rabbityLength[i]-30;
                  }  else{
                        rabbityLength[i]=rabbityLength[i-1];
                  }
                    if(rabbityLength[i]<70){
                      rabbityLength[i]=600;
                    }
                  }

              repaint();



        }else if (bottom) {

            for (int i = lengthOfFamily - 1; i >= 0; i--) {
                rabbitxLength[i + 1] = rabbitxLength[i];
            }
            for (int i = lengthOfFamily; i >= 0; i--) {
                if (i == 0) {
                    rabbityLength[i] = rabbityLength[i] + 30;
                } else {
                    rabbityLength[i] = rabbityLength[i - 1];
                }
                if (rabbityLength[i] > 600) {
                    rabbityLength[i] = 70;
                }
            }
            repaint();
        }
                                                         

    }

    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {
        if(moves==0){
            start = System.currentTimeMillis();
        }
        if(e.getKeyCode()== KeyEvent.VK_RIGHT){
            moves++;
            
            if(!left){
                right=true;
            }else{
             right=false;
             left=true;
            }
            top=false;
            bottom=false;
        }


         if(e.getKeyCode()== KeyEvent.VK_LEFT){
             moves++;

             if(!right){
                 left=true;
             }else{
              left=false;
              right=true;
             }
             top=false;
             bottom=false;
    }



         if(e.getKeyCode()== KeyEvent.VK_UP){
             moves++;

             if(!bottom){
                 top=true;
             }else{
              top=false;
              bottom=true;
             }
             left=false;
             right=false;
         }


         
          if(e.getKeyCode()== KeyEvent.VK_DOWN){
              moves++;

              if(!top){
                  bottom=true;
              }else{
               bottom=false;
               top=true;
              }
              left=false;
              right=false;
         }
    }

    public void keyReleased(KeyEvent e) {

    }
}
