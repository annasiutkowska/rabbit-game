import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        JFrame jFrame=new JFrame();
        Game game=new Game();

        jFrame.setBounds(12,12,900,800);
        jFrame.setBackground(Color.PINK);
        jFrame.setResizable(false);
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.add(game);
    }
}
